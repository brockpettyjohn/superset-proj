import { useEffect } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { embedDashboard } from '@superset-ui/embedded-sdk'

const dashboardUUID = "12272f42-acec-46eb-82af-b44e2de0f156"
const baseUrl = "http://10.30.50.114:8088"

function App() {
  const containerID = "container-id"

  useEffect(() => {
    embedDashboard({
      id: dashboardUUID, // given by the Superset embedding UI
      // id: "p04gybYo28v", // given by the Superset em0bedding UI
      supersetDomain: baseUrl,
      mountPoint: document.getElementById(containerID), // any html element that can contain an iframe
      fetchGuestToken: async () => fetchGuestToken(),
      // fetchGuestToken: async () => {
      //   return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoia3lsZSIsImZpcnN0X25hbWUiOiJreWxlIiwibGFzdF9uYW1lIjoiaHVyc3QifSwicmVzb3VyY2VzIjpbeyJ0eXBlIjoiZGFzaGJvYXJkIiwiaWQiOiI0ZDVkN2U3ZC01ZTkwLTQ1ZDktODViZi1kOTliNmNjMjc4MTQifV0sInJsc19ydWxlcyI6W3siY2xhdXNlIjoiKjoqIn1dLCJpYXQiOjE2ODA1NDYwNDcuMzg1NDI0NiwiZXhwIjoxNjgwNTQ2MzQ3LjM4NTQyNDYsImF1ZCI6Imh0dHBzOi8vc3VwZXJzZXQuYW1iZXJkYXRhLnRvb2xzLyIsInR5cGUiOiJndWVzdCJ9.eAAcro-hV4G4lE_lmwplGC45AjTwbnjaUYEyWE77lTs"
      // },
      debug: true,
      dashboardUiConfig: {
        // dashboard UI config: hideTitle, hideTab, hideChartControls, filters.visible, filters.expanded (optional)
        hideTitle: true,
        filters: {
          expanded: true,
          visible: true,
        },
      },
    })
  }, [])
  return (
    <div>
      <div
        id={containerID}
        // className="border  absolute top-0 right-0 left-0 bottom-0"
      ></div>
      {/* <iframe
        width="900"
        height="1200"
        seamless
        frameBorder="0"
        scrolling="no"
        src="http://10.30.50.114:8088/superset/explore/p/NRDvlpDvWjg/?standalone=1&height=400"
      ></iframe> */}
    </div>
  )
}

async function fetchGuestToken() {
  try {
    const body = {
      user: {
        username: "kyle",
        first_name: "kyle",
        last_name: "hurst",
        email: "kyle@amberdata.io",
      },
      resources: [
        {
          type: "dashboard",
          id: dashboardUUID,
        },
      ],
      rls: []
    }

    const loginResp = await fetch(
      `${baseUrl}/api/v1/security/login`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        mode: "cors",

        body: JSON.stringify({
          password: "Kyle",
          refresh: true,
          provider: "db",
          username: "kyle",
        }),
      }
    )
    const loginJSON = await loginResp.json()
    console.log("LOGIN ::  RESP", loginResp, loginJSON)
    if (loginJSON) {

      const guestResp = await fetch(
        `${baseUrl}/api/v1/security/guest_token/`,
        {
          method: "POST",
          headers: {
            "Authorization": `Bearer ${loginJSON.access_token}`,
            "Content-Type": "application/json",
          },
          mode: "cors",
          body: JSON.stringify(body),
        }
      )
      const guestJson = await guestResp.json()
      console.log("GUEST :: resp", guestResp)
      return guestJson.token
    }
    return ""
  } catch (err) {
    console.log(err)
  }
}

export default App